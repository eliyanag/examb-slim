<?php

require "bootstrap.php";
require "vendor/autoload.php";

use ExamB\Models\Car;
use ExamB\Middleware\Logging;

$app = new \Slim\App();
$app->add(new Logging());

//hello name---------------------------------------------------------------------------------------------------------
$app->get('/hello/{name}', function($request, $response,$args){
   return $response->write('Hello '.$args['name']);
});


//get all cars---------------------------------------------------------------------------------------------------------
$app->get('/cars', function($request, $response, $args){
    $_car = new Car(); //create new car
    $cars = $_car->all();
    $payload = [];
    foreach($cars as $car){
        $payload[$car->id] = [
            'manufacturer'=>$car->manufacturer,
            'model'=>$car->model
        ];
    }
    return $response->withStatus(200)->withJson($payload);
});

//---------------------------------------------------------------------------------------------------------------
$app->get('/cars/{id}', function($request, $response, $args){
    $_id = $args['id'];
    $car = Car::find($_id);
       return $response->withStatus(200)->withJson($car);
    });
//update----------------------------------------------------------------------------------------------------------------------
$app->put('/cars/{id}', function($request, $response,$args){
    $manufacturer = $request->getParsedBodyParam('manufacturer','');
    $model = $request->getParsedBodyParam('model','');
        $_car = Car::find($args['id']);
        $_car->manufacturer = $manufacturer;
        $_car->model = $model;

        if($_car->save()){ //אם הערך נשמר
            $payload = ['id' => $_car->id,"result" => "The car has been updates successfuly"];
            return $response->withStatus(200)->withJson($payload);
        }  
    else{
            return $response->withStatus(400);
        } 
});
//Delete
$app->delete('/cars/{id}', function($request, $response,$args){
    $car = Car::find($args['id']);
    $car->delete();
    
    if($car->exists){
        return $response->withStatus(400);
    }
    else{
       return $response->withStatus(200);
   }
});
/*Login without JWT
$app->post('/login', function($request, $response,$args){
    $manufacturer  = $request->getParsedBodyParam('manufacturer','');
    $model = $request->getParsedBodyParam('model','');    
    $_car = Car::where('manufacturer', '=', $manufacturer)->where('model', '=', $model)->get();
    
    if($_car[0]->id){
        $payload = ['success'=>true];
        return $response->withStatus(201)->withJson($payload)->withHeader('Access-Control-Allow-Origin', '*');
    }
    else{
        return $response->withStatus(400)->withHeader('Access-Control-Allow-Origin', '*');
    }
});*/
//Login with token
$app->post('/auth', function($request, $response,$args){
    $user = $request->getParsedBodyParam('user','');
    $password = $request->getParsedBodyParam('password','');
    
    if($user=='jack' && $password=='1234'){
        $payload = ['token'=>'1234'];
       // $basePath = rtrim(str_ireplace('index.php', '', $container['request']->getUri()->getBasePath()), '/users');
        return $response->withStatus(200)->withJson($payload); 
    }
    else{
        $payload = ['token'=>null];
        return $response->withStatus(403)->withJson($payload);
    }
});
    

//אבטחת מידע--------------------------------------------------------------------------------------------------------
$app->options('/{routes:.+}', function ($request, $response, $args) {
    return $response;
});

$app->add(function ($req, $res, $next) {
    $response = $next($req, $res);
    return $response
            ->withHeader('Access-Control-Allow-Origin', '*')
            ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
            ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
});

$app->run();