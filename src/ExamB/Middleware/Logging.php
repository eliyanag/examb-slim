<?php
namespace ExamB\Middleware;

class Logging {
    public function __invoke($request, $response, $next){
        //before route
        //ארור לוג כותב את הסטרינג לקובץ את רשימת הבקשות
        error_log($request->getMethod()."--".$request->getUri()."\r\n",3,"log.txt");
        $response = $next($request, $response); //בשביל SLIM
        //נקסט יודע לעבור למידל הבא או לראוט הבא
        return $response;
        //after route
    }
}